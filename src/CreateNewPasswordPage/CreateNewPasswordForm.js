import React from 'react';
import classes from './CreateNewPasswordForm.module.css';
import Input from '../Components/Input';

const CreateNewPasswordForm = ({values, errors, touched, handleChange}) => {
    return (
        <div className={classes.CreateNewPasswordForm}>
            <Input
                id='password'
                name='password'
                type='password'
                LabelText='Пароль:'
                placeholder='Введите пароль'
                value={values.password}
                onChange={handleChange}
                error={touched.password ? errors.password : null}
            />
            <Input
                id='repeatPassword'
                name='repeatPassword'
                type='password'
                LabelText='Подтверждение пароля:'
                placeholder='Введите пароль'
                value={values.repeatPassword}
                onChange={handleChange}
                error={touched.repeatPassword ? errors.repeatPassword : null}
            />
        </div>
    );
};

export default CreateNewPasswordForm;