import React, {useEffect} from 'react';
import classes from './LoginPage.module.css';
import {useFormik} from 'formik';
import LoginForm from './LoginForm';
import Button from '../Components/Button';
import * as actions from '../actions/actions';
import {connect} from 'react-redux';
import { Link, useHistory } from 'react-router-dom';

const mapStateToProps = (state) => {
    return {
        loginState: state.loginState,
        registrationWithGoogleState: state.registrationWithGoogleState,
    }
}

const actionsCreators = {
    loginRequest: actions.loginRequest,
    registrationWithGoogleRequest: actions.registrationWithGoogleRequest,
};

const validate = values => {
    const errors = {};

    if (!values.login) {
        errors.login = 'Required';
    }

    if (!values.password){
        errors.password = 'Required';
    } else if (!values.password.match(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]{8,})$/)) {
        errors.password = 'Password must be more than 8 symbols and contain upper and lower case letters and numbers';
    }

    return errors;
};


const LoginPage = ({loginState, loginRequest, registrationWithGoogleRequest}) => {

    const formik = useFormik({
        initialValues: {
            login: '',
            password: '',
        },
        validate,
        onSubmit: values => loginRequest(values),
    });

    const history = useHistory();

    useEffect(() => {
        if (loginState === 'failed') {
            formik.setErrors({
                password: 'Invalid data'
            })
        };

        if (loginState === 'finished') {
            history.push('/startpage');
        };
        
    }, [loginState, formik, history]);

    const onButtonGoogleClickHandler = (e) => {
        e.preventDefault();
        registrationWithGoogleRequest();
    }

    return (
        <form 
            className={classes.LoginPage}
            onSubmit={formik.handleSubmit}
        >
            <p className={classes.Text}>Авторизация:</p>
            <LoginForm 
                values={formik.values} 
                handleChange={formik.handleChange} 
                errors={formik.errors}
                touched={formik.touched}
            />
            <div>
                <Button
                    text='Войти'
                    type='submit'
                />
            </div>
            <div className={classes.Navigation}>
                <Button 
                    icon='google' 
                    iconColor='#2396b0' 
                    onClick={onButtonGoogleClickHandler}
                />
            </div>
            <div className={classes.Navigation}>
                <Link
                    className={classes.Text}
                    to='/registration'
                >Зарегистрироваться</Link>
                <Link
                    className={classes.Text}
                    to='/fogotpassword'
                >Забыли пароль?</Link>
            </div>
        </form>
    )
};

export default connect(mapStateToProps, actionsCreators)(LoginPage);
