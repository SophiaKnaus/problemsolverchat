import React, { useEffect, useState } from 'react';
import {BrowserRouter as Router, Route, Redirect, Switch} from 'react-router-dom';
import './App.css';
import LoginPage from './LoginPage/LoginPage';
import RegistrationPage from './RegistrationPage/RegistrationPage';
import 'font-awesome/css/font-awesome.css';
import FogotPasswordPage from './FogotPasswordPage/FogotPasswordPage';
import StartPage from './StartPage/StartPage';
import CreateNewPasswordPage from './CreateNewPasswordPage/CreateNewPasswordPage';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { isUserAuthorized, subscribeOnUserState } from './api/api';

const unAuthrorizedRoutes = {
  routes: [
    {
      path: '/login',
      Component: LoginPage,
    },
    {
      path: '/registration',
      Component: RegistrationPage,
    },
    {
      path: '/fogotpassword',
      Component: FogotPasswordPage,
    },
    {
      path: '/createnewpassword',
      Component: CreateNewPasswordPage,
    }
  ],
  defaultRedirect: '/login',
};

const authrorizedRoutes = {
  routes: [
    {
      path: '/startpage',
      Component: StartPage,
    },
  ],
  defaultRedirect: '/startpage',
};

function App() {
  const [isAuthorized, setIsAutorized] = useState(
    isUserAuthorized()
  );

  useEffect(() => {
    subscribeOnUserState((user) => {
      setIsAutorized(Boolean(user));
    });
  }, []);

  const routesInfo = isAuthorized ? authrorizedRoutes : unAuthrorizedRoutes;

  return (
    <div className="App">
      <Router>
        <Switch>
          {routesInfo.routes.map(({ path, Component }) => (
            <Route path={path}>
              <Component />
            </Route>
          ))}
          <Route path="/">
            <Redirect to={routesInfo.defaultRedirect} />
          </Route>
        </Switch>
      </Router>
      <ToastContainer/>
    </div>
  );
}

export default App;
