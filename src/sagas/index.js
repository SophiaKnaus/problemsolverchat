import { all, call } from '@redux-saga/core/effects';
import {loginWatchSaga} from './loginSaga';
import {registrationWatchSaga} from './registrationSaga';
import {registrationWithGoogleWatchSaga} from './registrationWithGoogleSaga';
import {sendResetPasswordEmailWatchSaga} from './sendResetPasswordEmailSaga';
import {createNewPasswordWatchSaga} from './createNewPasswordSaga';

export default function* rootSaga() {
    yield all([
      call(loginWatchSaga),
      call(registrationWatchSaga),
      call(registrationWithGoogleWatchSaga),
      call(sendResetPasswordEmailWatchSaga),
      call(createNewPasswordWatchSaga),
    ])
  }
  