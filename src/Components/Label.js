import React from 'react';
import classes from './Label.module.css';

const Label = ({htmlFor, text}) => {
    return (
        <label
            className={classes.Label}
            htmlFor={htmlFor}
        >
            {text}
        </label>
    )
}

export default Label;