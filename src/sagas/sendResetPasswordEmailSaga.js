import { call, put, takeLatest } from 'redux-saga/effects';
import {sendResetPasswordEmail} from '../api/api';
import {sendResetPasswordEmailRequest,sendResetPasswordEmailSuccess, sendResetPasswordEmailFailure} from '../actions/actions';

export function* sendResetPasswordEmailWatchSaga() {
    yield takeLatest(sendResetPasswordEmailRequest.toString(), sendResetPasswordEmailWorkerSaga);
}

function* sendResetPasswordEmailWorkerSaga(action) {
    try {
        const {email} = action.payload;
        yield call(sendResetPasswordEmail, email);
        yield put(sendResetPasswordEmailSuccess());
    } catch (e) {
        yield put(sendResetPasswordEmailFailure());
    }
}