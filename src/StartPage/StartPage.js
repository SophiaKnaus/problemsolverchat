import React from 'react';
import { signOut } from '../api/api';

const StartPage = () => {
    return (
        <div>
            <p>Start Page</p>
            <button onClick={signOut}>Sign out</button>
        </div>
    );
};

export default StartPage;