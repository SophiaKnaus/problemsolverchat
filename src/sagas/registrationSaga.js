import { call, put, takeLatest } from 'redux-saga/effects';
import {registrationIn} from '../api/api';
import {registrationRequest, registrationSuccess, registrationFailure} from '../actions/actions';

export function* registrationWatchSaga() {
    yield takeLatest(registrationRequest.toString(), registrationWorkerSaga);
};

export function* registrationWorkerSaga(action) {
    try {
        const { email, password } = action.payload;
        yield call(registrationIn, email, password);
        yield put(registrationSuccess());

    } catch(e) {
        yield put(registrationFailure());
    }

};