import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';
import * as actions from '../actions/actions';

const loginStateReducer = handleActions({
    [actions.loginRequest]() {
      return 'requested';
    },
    [actions.loginFailure]() {
      return 'failed';
    },
    [actions.loginSuccess]() {
      return 'finished';
    },
  }, 'none');

const registrationStateReducer = handleActions({
    [actions.registrationRequest]() {
      return 'requested';
    },
    [actions.registrationSuccess]() {
      return 'finished';
    },
    [actions.registrationFailure]() {
      return 'failed';
    },
}, 'none');

const registrationWithGoogleStateReducer = handleActions({
  [actions.registrationWithGoogleRequest]() {
    return 'requested';
  },
  [actions.registrationWithGoogleSuccess]() {
    return 'finished';
  },
  [actions.registrationWithGoogleFailure]() {
    return 'failed';
  },
}, 'none');

const sendResetPasswordEmailStateReducer = handleActions({
  [actions.sendResetPasswordEmailRequest]() {
    return 'requested';
  },
  [actions.sendResetPasswordEmailSuccess]() {
    return 'finished';
  },
  [actions.sendResetPasswordEmailFailure]() {
    return 'failed';
  },
}, 'none');

const createNewPasswordStateReducer = handleActions({
  [actions.createNewPasswordRequest]() {
    return 'requested';
  },
  [actions.createNewPasswordSuccess]() {
    return 'finished';
  },
  [actions.createNewPasswordFailure]() {
    return 'failed';
  },
}, 'none');

  export default combineReducers({
    loginState: loginStateReducer,
    registrationState: registrationStateReducer,
    registrationWithGoogleState: registrationWithGoogleStateReducer,
    sendResetPasswordEmailState: sendResetPasswordEmailStateReducer,
    createNewPasswordState: createNewPasswordStateReducer,
  });