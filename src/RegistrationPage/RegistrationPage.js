import React, {useEffect} from 'react';
import classes from './RegistrationPage.module.css';
import RegistrationForm from './RegistrationForm';
import Button from '../Components/Button';
import {useFormik} from 'formik';
import { Link } from 'react-router-dom';
import * as actions from '../actions/actions';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom';

const mapStateToProps = (state) => {
    return {
        registrationState: state.registrationState,
    }
};

const actionsCreators = {
    registrationRequest: actions.registrationRequest,
};


const validate = values => {
    const errors = {};

    if (!values.email) {
        errors.email = 'Required';
    }

    if (!values.password){
        errors.password = 'Required';
    } else if (!values.password.match(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]{8,})$/)){
        errors.password = 'Password must be more than 8 symbols and contain upper and lower case letters and numbers';
    }

    if(!values.repeatPassword){
        errors.repeatPassword = 'Required';
    } else if (values.repeatPassword !== values.password){
        errors.repeatPassword = 'Passwords do not match';
    } 

    return errors;
};

const RegistrationPage = ({registrationState, registrationRequest}) => {

    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
            repeatPassword: '',
        },
        validate,
        onSubmit: values => registrationRequest(values),
    });

    const history = useHistory();

    useEffect(() => {
        if (registrationState === 'failed') {
            formik.setErrors({
                repeatPassword: 'Invalid data'
            })
        }
        if(registrationState === 'finished') {
            history.push('/startpage');
        }

    }, [registrationState, formik, history]);

   
    

    return (
        <form 
            className={classes.RegistrationPage}
            onSubmit={formik.handleSubmit}
        >
            <p className={classes.Text}>Регистрация:</p>
            <RegistrationForm
                values={formik.values} 
                handleChange={formik.handleChange} 
                errors={formik.errors}
                touched={formik.touched}
            />
            <div>
                <Button
                    text='Регистрация'
                    type='submit'
                />
            </div>
            <div className={classes.Navigition}>
                <Link 
                    className={classes.Text}
                    to='/login'
                >Войти</Link>
                <Link 
                    className={classes.Text}
                    to='/fogotpassword'
                >Забыли пароль?</Link>
            </div>
        </form>
    );
}

export default  connect(mapStateToProps, actionsCreators)(RegistrationPage);