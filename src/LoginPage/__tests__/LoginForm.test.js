import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

Enzyme.configure({ adapter: new Adapter() });

import LoginForm from '../LoginForm';
import Input from '../../Components/Input';

describe('<LoginForm />', () => {
    it('renders two <Input> components', () => {
        const wrapper = shallow(<LoginForm values={{login: '', password: ''}} handleChange={jest.fn()} errors={{login:'', password:''}} touched={{login: false, password:false}} />);
        expect(wrapper.find(Input)).toHaveLength(2);
    });
});

