import {createAction} from 'redux-actions';

export const loginRequest = createAction('LOGIN_REQUEST');
export const loginSuccess = createAction('LOGIN_SUCCESS');
export const loginFailure = createAction('LOGIN_FAILURE');

export const registrationRequest = createAction('REGISTRATION_REQUEST');
export const registrationSuccess = createAction('REGISTRATION_SUCCESS');
export const registrationFailure = createAction('REGISTRATION_FAILURE');

export const registrationWithGoogleRequest = createAction('REGISTRATION_WITH_GOGLE_REQUEST');
export const registrationWithGoogleSuccess = createAction('REGISTRATION_WITH_GOGLE_SUCCESS');
export const registrationWithGoogleFailure = createAction('REGISTRATION_WITH_GOGLE_FAILURE');

export const sendResetPasswordEmailRequest = createAction('SEND_RESET_PASSWORD_EMAIL_REQUEST');
export const sendResetPasswordEmailSuccess = createAction('SEND_RESET_PASSWORD_EMAIL_SUCCESS');
export const sendResetPasswordEmailFailure = createAction('SEND_RESET_PASSWORD_EMAIL_FAILURE');

export const createNewPasswordRequest = createAction('CREATE_NEW_PASSWORD_REQUEST');
export const createNewPasswordSuccess = createAction('CREATE_NEW_PASSWORD_SUCCESS');
export const createNewPasswordFailure = createAction('CREATE_NEW_PASSWORD_FAILURE');