import { call, put, takeLatest } from 'redux-saga/effects';
import {registrationWithGoogle} from '../api/api';
import {registrationWithGoogleRequest, registrationWithGoogleSuccess, registrationWithGoogleFailure} from '../actions/actions';

export function* registrationWithGoogleWatchSaga() {
    yield takeLatest(registrationWithGoogleRequest.toString(), registrationWithGoogleWorkerSaga);
};

export function* registrationWithGoogleWorkerSaga() {
    try {
        yield call(registrationWithGoogle);
        yield put(registrationWithGoogleSuccess());

    } catch(e) {
        yield put(registrationWithGoogleFailure());
    }

};