import { initializeApp } from "firebase/app";
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, GoogleAuthProvider, signInWithRedirect, sendPasswordResetEmail, confirmPasswordReset, signOut as firebaseSignOut, onAuthStateChanged} from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyDXOe6mb1aqiKP6ZHO_ZMQj9R6VW-8IyFo",
    authDomain: "problemsolver-c8601.firebaseapp.com",
    databaseURL: "https://problemsolver-c8601-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "problemsolver-c8601",
    storageBucket: "problemsolver-c8601.appspot.com",
    messagingSenderId: "639962417860",
    appId: "1:639962417860:web:38c691a61797a5b3e0d791",
    measurementId: "G-65Y4DP6BEM"
  };
  
  // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const auth = getAuth(app);
    // const user = auth.currentUser;
    window.auth = auth;
    const googleProvider = new GoogleAuthProvider();
 


    export const signIn = async (email, password) => {
        return await signInWithEmailAndPassword(auth, email, password);
    }

    export const registrationIn = async (email, password) => {
      return await createUserWithEmailAndPassword(auth, email, password);
    }

    export const registrationWithGoogle = async () => {
      return await signInWithRedirect(auth, googleProvider); 
    }

    export const sendResetPasswordEmail = async (email) => {
      return await sendPasswordResetEmail(auth, email);
    }

    export const createNewPassword = async (code ,newPassword) => {
      return await confirmPasswordReset(auth, code, newPassword);
    }

    export const isUserAuthorized = () => {
      return Boolean(auth.currentUser);
    }

    export const subscribeOnUserState = (subscrber) => {
      onAuthStateChanged(auth, subscrber);
    }

    export const signOut = () => {
      firebaseSignOut(auth);
    }