import React from 'react';
import classes from './FogotPasswordForm.module.css';
import Input from '../Components/Input';

const FogotPasswordForm = ({values, errors, touched, handleChange}) => {
    return(
        <div className={classes.FogotPasswordForm}>
            <Input
                id='email'
                name='email'
                type='email'
                LabelText='Email:'
                placeholder='Введите email'
                value={values.email}
                onChange={handleChange}
                error={touched.email ? errors.email : null}
            />
        </div>
    );
}

export default FogotPasswordForm;