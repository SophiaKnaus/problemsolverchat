import React from 'react';
import classes from './Input.module.css';
import Label from './Label';

const Input = ({value, onChange, type, id, name, placeholder, LabelText, error}) => {
    return (
        <div className={classes.Container}>
            <div 
                className={classes.InputContainer}
            >
                <Label 
                    htmlFor={id}
                    text={LabelText}
                />
                <input
                    className={classes.Input}
                    type={type}
                    value={value}
                    onChange={onChange}
                    id={id}
                    name={name}
                    placeholder={placeholder}
                >
                </input>
            </div>
            {error ? <div className={classes.Error}>{error}</div> : null}
        </div>
    );
}

export default Input;