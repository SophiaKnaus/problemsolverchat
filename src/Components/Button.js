import React from 'react';
import classes from './Button.module.css';
import cn from 'classnames';


const iconToImageMap = {
    google: 'fa fa-google-plus-square fa-3x',
    twitter: 'fa fa-twitter-square fa-3x',
    facebook: 'fa fa-facebook-square fa-3x',
}

const Button = ({icon, onClick, text, type, iconColor}) => {
    const ButtonClass = cn({[classes.ButtonText] : text}, {[classes.ButtonIcon] : icon});
    const classImage = cn(iconToImageMap[icon]);

    return (
        <button 
            className={ButtonClass}
            onClick={onClick}
            type={type}
        >
            <i className={classImage} style={{'color': iconColor}}></i> {text}
        </button>
    );
}

export default Button;
