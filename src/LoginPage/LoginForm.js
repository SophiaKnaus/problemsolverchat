import React from 'react';
import classes from './LoginForm.module.css';
import Input from '../Components/Input';

const LoginForm = ({values, handleChange, errors, touched}) => {
    return (
        <div className={classes.LoginForm}>
                <Input 
                    id='login' 
                    name='login'
                    type='text'
                    LabelText='Login:'
                    placeholder='Введите логин'
                    value={values.login}
                    onChange={handleChange}
                    error={touched.login ? errors.login : null}
                />
                <Input
                    id='password'
                    name='password'
                    type='password'
                    LabelText='Password:'
                    placeholder='Введите пароль'
                    value={values.password}
                    onChange={handleChange}
                    error={touched.password ? errors.password : null}
                />
        </div>
    )
}

export default LoginForm;