import React, {useEffect} from 'react';
import classes from './CreateNewPasswordPage.module.css';
import Button from '../Components/Button';
import CreateNewPasswordForm from './CreateNewPasswordForm';
import {Link, useLocation, useHistory} from 'react-router-dom';
import {useFormik} from 'formik';
import * as actions from '../actions/actions';
import {connect} from 'react-redux';
import qs from 'query-string';
import {toast} from 'react-toastify';

const mapStateToProps = (state) => {
    return {
        createNewPasswordState: state.createNewPasswordState,
    }
};

const actionsCreators = {
    createNewPasswordRequest: actions.createNewPasswordRequest,
};

const validate = values => {
    const errors = {};

    if (!values.password){
        errors.password = 'Required';
    } else if (!values.password.match(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]{8,})$/)){
        errors.password = 'Password must be more than 8 symbols and contain upper and lower case letters and numbers';
    }

    if(!values.repeatPassword){
        errors.repeatPassword = 'Required';
    } else if (values.repeatPassword !== values.password){
        errors.repeatPassword = 'Passwords do not match';
    } 

    return errors;
};

const CreateNewPasswordPage = ({createNewPasswordState, createNewPasswordRequest}) => {

    const location = useLocation();

    const formik = useFormik({
        initialValues: {
            password: '',
            repeatPassword: '',
        },
        validate,
        onSubmit: values => {
            const {oobCode} = qs.parse(location.search);
            createNewPasswordRequest({oobCode,password:values.password})
        },
    });

    const history = useHistory();

    useEffect(() => {
        if (createNewPasswordState === 'failed') {
            formik.setErrors({
                repeatPassword: 'Invalid data'
            })
        }

        if (createNewPasswordState === 'finished') {
            toast('Your password was changed', {
                position: 'bottom-right',
                type: 'success',
            });
            history.push('/startpage');
        }

    }, [createNewPasswordState, formik, history]);

    return (
        <form 
            className={classes.CreateNewPasswordPage}
            onSubmit={formik.handleSubmit}
        >
            <p className={classes.Text}>Обновление пароля:</p>
            <CreateNewPasswordForm
                values={formik.values} 
                handleChange={formik.handleChange} 
                errors={formik.errors}
                touched={formik.touched}
            />
            <div>
                <Button
                    type='submit'
                    text='Сменить пароль'
                />
            </div>
            <div className={classes.Navigation}>
                <Link 
                    className={classes.Text} 
                    to='/login'
                >
                    Войти
                </Link>
                <Link 
                    className={classes.Text} 
                    to='/registration'
                >
                    Регистрация
                </Link>
            </div>
        </form>
    );
};

export default connect(mapStateToProps, actionsCreators)(CreateNewPasswordPage);