import { call, put, takeLatest } from 'redux-saga/effects';
import {signIn} from '../api/api';
import {loginSuccess,loginFailure, loginRequest} from '../actions/actions';

export function* loginWatchSaga() {
    yield takeLatest(loginRequest.toString(), loginWorkerSaga);
}

function* loginWorkerSaga(action) {
    try {
        const {login, password} = action.payload;
        yield call(signIn, login, password);
        yield put(loginSuccess());
    } catch (e) {
        yield put(loginFailure());
    }
}
