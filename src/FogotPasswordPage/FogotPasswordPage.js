import React, {useEffect} from 'react';
import classes from './FogotPasswordPage.module.css';
import FogotPasswordForm from './FogotPasswordForm';
import Button from '../Components/Button';
import {Link} from 'react-router-dom';
import {useFormik} from 'formik';
import * as actions from '../actions/actions';
import {connect} from 'react-redux';
import {toast} from 'react-toastify';

const mapStateToProps = (state) => {
    return {
        sendResetPasswordEmailState: state.sendResetPasswordEmailState,
    }
};

const actionsCreators = {
    sendResetPasswordEmailRequest: actions.sendResetPasswordEmailRequest,
};

const validate = values => {
    const errors = {};

    if (!values.email) {
        errors.email = 'Required';
    }

    return errors;
};


const FogotPasswordPage = ({sendResetPasswordEmailState, sendResetPasswordEmailRequest}) => {

    const formik = useFormik({
        initialValues: {
            email: '',
        },
        validate,
        onSubmit: values => sendResetPasswordEmailRequest(values),
    });

    useEffect(() => {
        if (sendResetPasswordEmailState === 'failed') {
            formik.setErrors({
                email: 'Invalid data'
            })
        }

        if (sendResetPasswordEmailState === 'finished') {
            toast('Message was send to your email', {
                position: 'bottom-right',
                type: 'success',
            })
        }

    }, [sendResetPasswordEmailState, formik]);

    return (
        <form 
            className={classes.FogotPasswordPage}
            onSubmit={formik.handleSubmit}
        >
            <p className={classes.Text}>Забыли пароль:</p>
            <FogotPasswordForm
                values={formik.values} 
                handleChange={formik.handleChange} 
                errors={formik.errors}
                touched={formik.touched}
            />
            <div>
                <Button 
                    text='Отправить ссылку для восстановления' 
                    type='submit'
                />
            </div>
            <div className={classes.Navigation}>
                <Link 
                    className={classes.Text}
                    to='/login'
                >Войти</Link>
                <Link
                    className={classes.Text}
                    to='/registration'
                >Регистрация</Link>
            </div>
        </form>
    );
};

export default connect(mapStateToProps, actionsCreators)(FogotPasswordPage);