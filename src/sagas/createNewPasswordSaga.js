import { call, put, takeLatest } from 'redux-saga/effects';
import {createNewPassword} from '../api/api';
import {createNewPasswordSuccess,createNewPasswordFailure, createNewPasswordRequest} from '../actions/actions';

export function* createNewPasswordWatchSaga() {
    yield takeLatest(createNewPasswordRequest.toString(), createNewPasswordWorkerSaga);
}

function* createNewPasswordWorkerSaga(action) {
    try {
        const {oobCode , password} = action.payload;
        yield call(createNewPassword, oobCode, password);
        yield put(createNewPasswordSuccess());
    } catch (e) {
        yield put(createNewPasswordFailure());
    }
}